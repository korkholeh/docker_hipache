FROM phusion/baseimage:0.9.10

RUN /etc/my_init.d/00_regen_ssh_host_keys.sh
CMD ["/sbin/my_init"]

RUN apt-get -qq update
RUN apt-get -y install wget psmisc mc daemontools curl
RUN apt-get -y install supervisor nodejs npm redis-server
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Then install it
RUN npm install -g hipache --production

RUN mkdir -p /root/config
ADD hipache.json /root/config/hipache.json
ADD redis.conf /root/config/redis.conf

RUN mkdir /etc/service/redis
ADD redis.sh /etc/service/redis/run

RUN mkdir /etc/service/hipache
ADD hipache.sh /etc/service/hipache/run

# This is provisional, as we don't honor it yet in hipache
ENV NODE_ENV production

VOLUME /var/log/
VOLUME /var/lib/redis/
VOLUME /root/.ssh
VOLUME /root/config/

EXPOSE 80
EXPOSE 6379
